import React, { useState, useEffect, useReducer, useRef } from 'react';
import logo from './logo.svg';
import './App.css';

// Functions for calculating area, perimeter, and volume
function calcArea(l, w) {
  return l * w;
}

function calcPerimeter(l, w) {
  return 2 * (parseInt(l) + parseInt(w));
}

function calcVolume(l, w, h) {
  return l * w * h;
}

function init(initialCount) {
  return {val: initialCount};
}

function reducer(state, action) {
  debugger;
  switch (action.type) {
    case 'submit':
      return {length: action.length,
              width: action.width,
              height: action.height}
    case 'area':
      return {length: state.length,
              width: state.width,
              height: state.height,
              area: calcArea(state.length, state.width),
              perimeter: state.perimeter || 0,
              volume: state.volume || 0};
    case 'perimeter':
      return {length: state.length,
              width: state.width,
              height: state.height,
              area: state.area || 0,
              perimeter: calcPerimeter(state.length, state.width),
              volume: state.volume || 0};
    case 'volume':
      return {length: state.length,
              width: state.width,
              height: state.height,
              area: state.area || 0,
              perimeter: state.perimeter || 0,
              volume: calcVolume(state.length, state.width, state.height)};
    case 'reset':
      return init(action.payload);
    default:
      throw new Error();
  }
}

function App({initialCount}) {

  const initialState = {length: 0,
                        width: 0,
                        height: 0,
                        area: 0, 
                        perimeter: 0,
                        volume: 0};

  const userLength = useRef();
  const userWidth = useRef();
  const userHeight = useRef();
  const [state, dispatch] = useReducer(reducer, initialState);

  function handleSubmit(e) {
    debugger;
    e.preventDefault();
    dispatch({
      type: 'submit',
      length: userLength.current.value,
      width: userWidth.current.value,
      height: userHeight.current.value,
    });
    /*userLength.current.value = 0;
    userWidth.current.value = 0;
    userHeight.current.value = 0;*/
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>
          Pool Cost Estimator 
        </h1>
        <a
          className="App-link"
          href="https://www.liquiddesignacc.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Liquid Design Aquatic Consulting Company
        </a>
      </header>
      <div className="App-body">
        <div className="Display-dims">
          <h3>Length: {state.length || 'Not set'}</h3>
          <h3>Width: {state.width || 'Not set'}</h3>
          <h3>Height: {state.height || 'Not set'}</h3>
        </div>
        <div className="Form-dims">
          <form onSubmit={handleSubmit}>
            <label>Length: </label>
            <input type="number" ref={userLength} required />
            <label>Width: </label>
            <input type="number" ref={userWidth} required />
            <label>Height: </label>
            <input type="number" ref={userHeight} required />
            <button type="submit">Set Pool Dims</button>
          </form>
        </div>
        <div className="Calc-dims"> 
          <div className="Calc-buttons">
            <h2>Area (sq): {state.area || 'Not calculated'}</h2>
            <button 
              onClick={() => dispatch({type: 'area'})}>
              Area
            </button>
          </div>
          <div className="Calc-buttons">
            <h2>Perimeter: {state.perimeter || 'Not calculated'}</h2>
            <button
              onClick={() => dispatch({type: 'perimeter'})}>
              Perimeter
            </button>
          </div>
          <div className="Calc-buttons">
            <h2>Volume (cubed): {state.volume || 'Not calculated'}</h2>
            <button
              onClick={() => dispatch({type: 'volume'})}>
              Volume
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
