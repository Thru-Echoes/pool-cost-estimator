import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders Liquid Design link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Liquid Design Aquatic Consulting Company/i);
  expect(linkElement).toBeInTheDocument();
});
